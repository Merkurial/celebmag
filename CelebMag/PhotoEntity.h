//
//  PhotoEntity.h
//  CelebMag
//
//  Created by minhnh on 12/5/14.
//  Copyright (c) 2014 minhnh. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PersonEntity;

@interface PhotoEntity : NSObject

@property (nonatomic, strong) NSString *photoId;
@property (nonatomic, assign) NSUInteger personIdRef;
@property (nonatomic, strong) PersonEntity *person;
@property (nonatomic, strong) NSString *photoFilename;
@property (nonatomic, readonly) NSString *urlThumbnail;
@property (nonatomic, readonly) NSString *urlMedium;
@property (nonatomic, readonly) NSString *urlOriginal;
@property (nonatomic, assign) BOOL isNew;

- (instancetype)initWithPhotoId:(NSString *)photoId personIdRef:(NSUInteger)personIdRef filename:(NSString *)photoFilename thumbnail:(NSString *)thumbnail person:(PersonEntity *)person;

@end
