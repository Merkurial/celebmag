//
//  MoreAppViewCell.h
//  CelebMag
//
//  Created by minhnh on 7/4/15.
//  Copyright (c) 2015 minhnh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreAppViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *appThumbImage;
@property (weak, nonatomic) IBOutlet UILabel *appLabelName;

@end
