//
//  DatabaseHelper.m
//  GenerateDB
//
//  Created by minhnh on 11/8/14.
//  Copyright (c) 2014 minhnh. All rights reserved.
//

#import "DatabaseHelper.h"
#import "PhotoEntity.h"
#import "PersonEntity.h"
#import "Constant.h"
#import "FMDB.h"
#import "NSArray+Shuffle.h"
#import <AFNetworking.h>
#import <SVProgressHUD.h>
#import <PDKTZipArchive.h>

@implementation DatabaseHelper {
	FMDatabase *database;
}

#pragma mark - Init

- (instancetype)init {
	self = [super init];
	if (self) {
//		[self checkUpdate];
	}

	return self;
}

- (NSString *)pathForDBExtract {
	NSString *path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:@"db.sqlite"];
	NSLog(@"%@", path);
	return path;
}

- (NSString *)pathForDB {
    NSString *path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:@"db.sqlite/db.sqlite"];
    NSLog(@"%@", path);
    return path;
}

- (NSString *)pathForDBZipFile {
    NSString *path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:@"db.sqlite.zip"];
    NSLog(@"%@", path);
    return path;
}

+ (instancetype)sharedInstance {
	static dispatch_once_t once;
	static DatabaseHelper *singleton = nil;

	dispatch_once(&once, ^{
		singleton = [[self alloc] init];
	});

	return singleton;
}

- (void)checkUpdateWithCompletionHandler:(id<FinishUpdatingDelegate>)delegate {
	NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"UPDATE_TIME"];
	if (![[NSFileManager defaultManager] fileExistsAtPath:[self pathForDB]] || [savedValue  isEqual: @""]) {
		// Download request
		[self downloadDatabaseWithDelegate:delegate];
	} else {
		NSURLRequest *request2 = [NSURLRequest requestWithURL:[NSURL URLWithString:CHECK_UPDATE_LINK] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10];
		AFHTTPRequestOperation *noteOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request2];
		noteOperation.responseSerializer = [AFJSONResponseSerializer serializer];
		[noteOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
			NSDictionary *dict = (NSDictionary *)[(NSArray *)responseObject firstObject];
			NSString *value = [dict valueForKeyPath:@"updated_at"];
			if ([value isEqualToString:savedValue]) {
				database = [FMDatabase databaseWithPath:[self pathForDB]];
				[database open];
				[delegate finishingUpdateWorkAndNeedToDownload:NO];
			} else {
				[self downloadDatabaseWithDelegate:delegate];
			}
		} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            database = [FMDatabase databaseWithPath:[self pathForDB]];
            [database open];
            [delegate finishingUpdateWorkAndNeedToDownload:NO];
		}];
		
		[noteOperation start];
	}
}

- (void)downloadDatabaseWithDelegate:(id<FinishUpdatingDelegate>)delegate {
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://www.dropbox.com/s/br2sl2264vy27br/db.sqlite.zip?dl=1"]];
	AFHTTPRequestOperation *downloadOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
	downloadOperation.outputStream = [NSOutputStream outputStreamToFileAtPath:[self pathForDBZipFile] append:NO];
	[downloadOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [PDKTZipArchive unzipFileAtPath:[self pathForDBZipFile] toDestination:[self pathForDBExtract]];
		database = [FMDatabase databaseWithPath:[self pathForDB]];
		[database open];
		[delegate finishingUpdateWorkAndNeedToDownload:YES];
	} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
		[delegate didFailedToUpdate];
	}];
	
	[downloadOperation start];
}

#pragma mark - Public methods

- (PersonEntity *)getPersonEntityFromPhotoEntity:(PhotoEntity *)photoEntity {
	FMResultSet *resultSet = [database executeQuery:@"SELECT * FROM people WHERE id = ?", [NSNumber numberWithUnsignedInteger:photoEntity.personIdRef]];
	[resultSet next];
	NSUInteger personId = [resultSet intForColumn:@"id"];
	NSString *name = [resultSet stringForColumn:@"name"];
	NSString *biography = [resultSet stringForColumn:@"bio"];

	PersonEntity *personEntity = [[PersonEntity alloc] initWithPersonId:personId name:name biography:biography];

	return personEntity;
}

- (NSArray *)getNewUpdatedPhotos {
	NSMutableArray *array = [NSMutableArray new];
	
	FMResultSet *resultSet = [database executeQuery:@"SELECT * FROM photos WHERE is_new = 1"];
	while ([resultSet next]) {
		int personId = [resultSet intForColumn:@"person_id"];
		PersonEntity *personEntity = [self getPersonBasedOnId:personId];
		NSString *photoId = [resultSet stringForColumn:@"id"];
		NSString *filename = [resultSet stringForColumn:@"filename"];
		NSString *photoThumbnail = [resultSet stringForColumn:@"thumbnail"];
		PhotoEntity *entity = [[PhotoEntity alloc] initWithPhotoId:photoId personIdRef:personId filename:filename thumbnail:photoThumbnail person:personEntity];
		entity.isNew = YES;

		[array addObject:entity];
	}
	
	return [array shuffledArray];
}

- (NSArray *)getNewPhotoForPerson:(PersonEntity *)personEntity {
	NSMutableArray *array = [NSMutableArray new];
	
	FMResultSet *resultSet = [database executeQuery:@"SELECT * FROM photos WHERE person_id = ? AND is_new = 1", [NSNumber numberWithUnsignedInteger:personEntity.personId]];
	while ([resultSet next]) {
		NSString *photoId = [resultSet stringForColumn:@"id"];
		NSString *filename = [resultSet stringForColumn:@"filename"];
		NSString *photoThumbnail = [resultSet stringForColumn:@"thumbnail"];
		PhotoEntity *entity = [[PhotoEntity alloc] initWithPhotoId:photoId personIdRef:personEntity.personId filename:filename thumbnail:photoThumbnail person:personEntity];
		entity.isNew = YES;
		
		[array addObject:entity];
	}
	
	return [array shuffledArray];
}

- (PersonEntity *)getPersonBasedOnId:(int)personId {
	FMResultSet *resultSet = [database executeQuery:@"SELECT * FROM people WHERE id = ?", [NSNumber numberWithInt:personId]];
	PersonEntity *personEntity;
	
	if ([resultSet next]) {
		NSString *name = [resultSet stringForColumn:@"name"];
		NSString *biography = [resultSet stringForColumn:@"bio"];
		personEntity = [[PersonEntity alloc] initWithPersonId:personId name:name biography:biography];
	}
	return personEntity;
}

- (NSArray *)getRandomPeople {
	FMResultSet *resultSet = [database executeQuery:@"SELECT * FROM people"];
	NSMutableArray *people = [[NSMutableArray alloc] init];

	while ([resultSet next]) {
		NSUInteger personId = [resultSet intForColumn:@"id"];
		NSString *name = [resultSet stringForColumn:@"name"];
		NSString *biography = [resultSet stringForColumn:@"bio"];
		PersonEntity *personEntity = [[PersonEntity alloc] initWithPersonId:personId name:name biography:biography];

		[people addObject:personEntity];
	}

	return [people shuffledArray];
}

- (NSArray *)getSortedPeople {
    FMResultSet *resultSet = [database executeQuery:@"SELECT * FROM people ORDER BY name"];
    NSMutableArray *people = [[NSMutableArray alloc] init];
    
    while ([resultSet next]) {
        NSUInteger personId = [resultSet intForColumn:@"id"];
        NSString *name = [resultSet stringForColumn:@"name"];
        NSString *biography = [resultSet stringForColumn:@"bio"];
        PersonEntity *personEntity = [[PersonEntity alloc] initWithPersonId:personId name:name biography:biography];
        
        [people addObject:personEntity];
    }
    
    return [people copy];
}

- (NSInteger)getTotalNumOfPhotosOfPerson:(PersonEntity *)person {
    NSString *query = [NSString stringWithFormat:@"select count(id) from photos where photos.person_id=%lu", (unsigned long)person.personId];
    return [database intForQuery:query];
}

- (NSArray *)getPeopleThatHaveUpdatedPhotosAndNameContain:(NSString *)name {
	NSMutableArray *arrayResult = [NSMutableArray new];
	NSString *query;
	
	if ([name isEqualToString:@""]) {
		query = [NSString stringWithFormat:@"SELECT people.* from people, photos where people.id=photos.person_id and photos.is_new=1 group by people.id ORDER BY name"];
	} else {
		query = [NSString stringWithFormat:@"SELECT people.* from people, photos where people.id=photos.person_id and photos.is_new=1 and people.name like '%%%@%%' group by people.id ORDER BY name", name];
	}
	FMResultSet *resultSet = [database executeQuery:query];
	
	while ([resultSet next]) {
		NSUInteger personId = [resultSet intForColumn:@"id"];
		NSString *name = [resultSet stringForColumn:@"name"];
		NSString *biography = [resultSet stringForColumn:@"bio"];
		PersonEntity *personEntity = [[PersonEntity alloc] initWithPersonId:personId name:name biography:biography];
		
		[arrayResult addObject:personEntity];
	}
	
	return [arrayResult copy];
}

- (NSArray *)getPeopleListFromGivenName:(NSString *)name {
	NSMutableArray *arrayResult = [NSMutableArray new];

	NSString *query = [NSString stringWithFormat:@"SELECT * FROM people WHERE name LIKE '%%%@%%'", name];
	FMResultSet *resultSet = [database executeQuery:query];

	while ([resultSet next]) {
		NSUInteger personId = [resultSet intForColumn:@"id"];
		NSString *name = [resultSet stringForColumn:@"name"];
		NSString *biography = [resultSet stringForColumn:@"bio"];
		PersonEntity *personEntity = [[PersonEntity alloc] initWithPersonId:personId name:name biography:biography];

		[arrayResult addObject:personEntity];
	}

	return [arrayResult copy];
}

- (NSArray *)getRandomImagesFromPeopleArray:(NSArray *)peopleArray {
	NSMutableArray *arrayResult = [[NSMutableArray alloc] init];

	for (PersonEntity *personEntity in peopleArray) {
        [arrayResult addObjectsFromArray:[self photosFromQuery:[NSString stringWithFormat:@"SELECT * FROM photos WHERE person_id = %lu AND is_new = 1 ORDER BY RANDOM() LIMIT %d", (unsigned long)personEntity.personId, NUM_OF_RND_IMG] person:personEntity]];
        
        if ([arrayResult count] < NUM_OF_RND_IMG) {
            [arrayResult addObjectsFromArray:[self photosFromQuery:[NSString stringWithFormat:@"SELECT * FROM photos WHERE person_id = %lu AND flag = 0 ORDER BY RANDOM() LIMIT %lu", (unsigned long)personEntity.personId, NUM_OF_RND_IMG - [arrayResult count]] person:personEntity]];
        }
        

		// When random images got are less than expected, we need to reset the flag
		if ([arrayResult count] < NUM_OF_RND_IMG) {
			NSMutableArray *arrayOfGotPhotosId = [NSMutableArray new];
			for (PhotoEntity *photo in arrayResult) {
				[arrayOfGotPhotosId addObject:photo.photoId];
			}
			NSString *stringFromGotPhotosIdArray = [arrayOfGotPhotosId componentsJoinedByString:@","];

			[database executeUpdate:@"UPDATE photos SET flag = 0"];

			NSUInteger photosLeft = NUM_OF_RND_IMG - [arrayResult count];
			NSString *query = [NSString stringWithFormat:@"SELECT * FROM photos WHERE person_id = %lu AND flag = 0 AND id NOT IN (%@) ORDER BY RANDOM() LIMIT %lu", (unsigned long)personEntity.personId, stringFromGotPhotosIdArray, (unsigned long)photosLeft];
			[arrayResult addObjectsFromArray:[self photosFromQuery:query person:personEntity]];
		}
	}

	return [arrayResult shuffledArray];
}

- (NSArray *)getRandomImagesOfPerson:(PersonEntity *)person {
	NSArray *newPhotosArray = [[self photosFromQuery:[NSString stringWithFormat:@"SELECT * FROM photos WHERE person_id = %lu AND flag = 0", person.personId] person:person] shuffledArray];
	NSArray *oldPhotosArray = [[self photosFromQuery:[NSString stringWithFormat:@"SELECT * FROM photos WHERE person_id = %lu AND flag = 1", person.personId] person:person] shuffledArray];
	NSMutableArray *arrayResult = [[NSMutableArray alloc] initWithArray:newPhotosArray];
	[arrayResult addObjectsFromArray:oldPhotosArray];
	
	return [arrayResult copy];
}

- (PhotoEntity *)getPhotoBasedOnPerson:(PersonEntity *)person {
	NSString *query = [NSString stringWithFormat:@"SELECT * FROM photos WHERE person_id = %lu LIMIT 1", (unsigned long)person.personId];

	FMResultSet *result = [database executeQuery:query];
	[result next];
	NSString *photoFilename = [result stringForColumn:@"filename"];
	NSString *photoId = [[NSString alloc] initWithString:photoFilename];
	NSUInteger personId = [result longLongIntForColumn:@"person_id"];
	NSString *photoThumbnail = [result stringForColumn:@"thumbnail"];
	PhotoEntity *entity = [[PhotoEntity alloc] initWithPhotoId:photoId personIdRef:personId filename:photoFilename thumbnail:photoThumbnail person:person];

	return entity;
}

- (NSArray *)photosFromQuery:(NSString *)query person:(PersonEntity *)person {
	FMResultSet *resultSet = [database executeQuery:query];
	return [self photosFromResultSet:resultSet person:person];
}

- (NSArray *)photosFromResultSet:(FMResultSet *)resultSet person:(PersonEntity *)person {
	NSMutableArray *arrayResult = [[NSMutableArray alloc] init];

	while ([resultSet next]) {
		NSUInteger personId = [resultSet intForColumn:@"person_id"];
		NSString *photoId = [resultSet stringForColumn:@"id"];
		NSString *filename = [resultSet stringForColumn:@"filename"];
		NSString *photoThumbnail = [resultSet stringForColumn:@"thumbnail"];
		NSInteger isNewInt = [resultSet intForColumn:@"is_new"];
		PhotoEntity *entity = [[PhotoEntity alloc] initWithPhotoId:photoId personIdRef:personId filename:filename thumbnail:photoThumbnail person:person];
		entity.isNew = isNewInt == 0 ? NO : YES;

		[arrayResult addObject:entity];
	}
	return arrayResult;
}

- (NSArray *)getOccupationBasedOnPerson:(PersonEntity *)person {
	NSMutableArray *arrayResult = [NSMutableArray new];
	NSString *query = [NSString stringWithFormat:@"SELECT * FROM occupation WHERE person_id = %lu", (unsigned long)person.personId];

	FMResultSet *result = [database executeQuery:query];
	while ([result next]) {
		NSString *occupation = [result stringForColumn:@"occupation"];
		[arrayResult addObject:occupation];
	}

	return [arrayResult copy];
}

- (void)updateFlagToBeRead:(PhotoEntity *)entity {
	NSString *query = [NSString stringWithFormat:@"UPDATE photos SET flag = 1, is_new = 0 WHERE id = '%@'", entity.photoId];
	[database executeUpdate:query];
}

@end
