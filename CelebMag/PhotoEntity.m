//
//  PhotoEntity.m
//  CelebMag
//
//  Created by minhnh on 12/5/14.
//  Copyright (c) 2014 minhnh. All rights reserved.
//

#import "PhotoEntity.h"
#import "Constant.h"
#import "PersonEntity.h"

@implementation PhotoEntity

- (instancetype)initWithPhotoId:(NSString *)photoId personIdRef:(NSUInteger)personIdRef filename:(NSString *)photoFilename thumbnail:(NSString *)thumbnail person:(PersonEntity *)person
{
	self = [super init];
	if (self) {
		self.photoId = photoId;
		self.personIdRef = personIdRef;
		self.photoFilename = photoFilename;
        self.person = person;
		self.isNew = NO;
	}
	
	return self;
}

- (NSString *)urlOriginal
{
	return [NSString stringWithFormat:@"%@%@", LINK_SERVER_IMAGE_ORIGINAL, self.photoFilename];
}

- (NSString *)urlThumbnail
{
	return [NSString stringWithFormat:@"%@%@", LINK_SERVER_IMAGE_THUMB, self.photoFilename];
}

- (NSString *)urlMedium
{
	return [NSString stringWithFormat:@"%@%@", LINK_SERVER_IMAGE_MEDIUM, self.photoFilename];
}

@end
