//
//  DrawMenuCell.m
//  CelebMag
//
//  Created by minhnh on 11/6/15.
//  Copyright © 2015 minhnh. All rights reserved.
//

#import "DrawMenuCell.h"

@implementation DrawMenuCell

- (void)awakeFromNib {
    // Initialization code
	self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"default-bg.jpg"]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
