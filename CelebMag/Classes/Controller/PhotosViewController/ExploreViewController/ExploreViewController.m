//
//  ExploreViewController.m
//  CelebMag
//
//  Created by Dung Nguyen on 1/16/15.
//  Copyright (c) 2015 minhnh. All rights reserved.
//

#import "ExploreViewController.h"
#import "PersonViewController.h"

#import <AFNetworking.h>

#import "ExplorePhotoCell.h"
#import <UIImageView+WebCache.h>
#import <SDImageCache.h>
#import <SVProgressHUD.h>
#import <CLImageEditor.h>
#import <UIImage+SVG.h>

@interface ExploreViewController () <UIAlertViewDelegate, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, FinishUpdatingDelegate, CLImageEditorDelegate>

@property (nonatomic, strong) NSMutableArray *photosArray;
@property (weak, nonatomic) IBOutlet UICollectionView *mainCollectionView;

@end

@implementation ExploreViewController

// TODO: Re-implement using lazy instantiation

static NSString *const ExploreShowPersonSegueIdentifier = @"ExploreShowPerson";
static BOOL isFetchedFromRemoteServer = NO;

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.
    
	// Initiate the search text field
    self.mainCollectionView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"default-bg.jpg"]];

	if (!isFetchedFromRemoteServer) {
		isFetchedFromRemoteServer = !isFetchedFromRemoteServer;
		[SVProgressHUD showWithStatus:@"Updating data, it may take a while..."];
		[[DatabaseHelper sharedInstance] checkUpdateWithCompletionHandler:self];
	} else {
		[SVProgressHUD showWithStatus:@"Loading data..."];
		[self fetchPhotoToCollectionView];
	}
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	if ([segue.identifier isEqualToString:ExploreShowPersonSegueIdentifier]) {
		PersonViewController *vc = segue.destinationViewController;
		vc.person = sender;
    } else if ([segue.identifier isEqualToString:@"ShowPerson"]) {
        PersonViewController *pvc = (PersonViewController *)segue.destinationViewController;
        
        NSIndexPath *selectedIndexPath = [self.mainCollectionView indexPathForCell:sender];
        PhotoEntity *photoEntity = self.photosArray[selectedIndexPath.row];
        
        pvc.photoToScrollTo = photoEntity;
        pvc.person = photoEntity.person;
    }
}

#pragma mark - DataManager delegate

- (void)finishingUpdateWorkAndNeedToDownload:(BOOL)isNeedToDownload {
	// Save to UserDefault
	if (isNeedToDownload) {
		NSURLRequest *request2 = [NSURLRequest requestWithURL:[NSURL URLWithString:CHECK_UPDATE_LINK]];
		AFHTTPRequestOperation *noteOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request2];
		noteOperation.responseSerializer = [AFJSONResponseSerializer serializer];
		[noteOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
			NSDictionary *dict = (NSDictionary *)[(NSArray *)responseObject firstObject];
			NSString *valueToSave = [dict valueForKeyPath:@"updated_at"];
			[[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"UPDATE_TIME"];
			[[NSUserDefaults standardUserDefaults] synchronize];
            
            // If users currently navigating the person category view, it might need to be reloaded
            [[NSNotificationCenter defaultCenter] postNotificationName:@"PersonCategoryTableViewWillReload" object:self];
		} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
			[[NSNotificationCenter defaultCenter] postNotificationName:@"PersonCategoryTableViewWillReload" object:self];
		}];
		
		[noteOperation start];
    } else {
        // Just fire the notification
        [[NSNotificationCenter defaultCenter] postNotificationName:@"PersonCategoryTableViewWillReload" object:self];
    }
	
	[self fetchPhotoToCollectionView];
}

- (void)didFailedToUpdate {
	[SVProgressHUD dismiss];
	[[[UIAlertView alloc] initWithTitle:@"Failed to connect to database" message:@"There was an error connecting to database. Please check your network connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

#pragma mark - Image editor delegates

- (void)imageEditor:(CLImageEditor *)editor didFinishEdittingWithImage:(UIImage *)image {
	[SVProgressHUD showWithStatus:@"Saving image"];
	UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
}

- (void)imageEditorDidCancel:(CLImageEditor *)editor {
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
	if (error) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
										   message:@"Unable to save image to Photo Album."
										  delegate:self cancelButtonTitle:@"Ok"
								 otherButtonTitles:nil];
		[alert show];
	} else {
		[SVProgressHUD dismiss];
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Successfully" message:@"Saved image successfully. Please switch to your Photos/Gallery app to set it as your homescreen/lockscreen." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
	}
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
	[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Collection view delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.photosArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ExplorePhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ExploreCell" forIndexPath:indexPath];
    
    if (cell) {
        PhotoEntity *photoEntity = self.photosArray[indexPath.row];
        
        cell.downloadIconImage.image = [UIImage imageWithSVGNamed:@"download-icon" targetSize:cell.downloadIconImage.frame.size fillColor:[UIColor whiteColor]];
        cell.categoryName.text = photoEntity.person.name;
        
        cell.imageLoader.lineWidth = 5;
        cell.imageLoader.color = [UIColor colorWithRed:0.133 green:0.663 blue:0.894 alpha:1.000];
        [cell.imageLoader startAnimation];
        
        [cell.mainImage sd_setImageWithURL:[NSURL URLWithString:photoEntity.urlThumbnail] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            cell.mainImage.image = image;
            [cell.imageLoader stopAnimation];
            [cell.imageLoader removeFromSuperview];
        }];
    }
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        float itemSize = self.mainCollectionView.frame.size.width / 4;
        return CGSizeMake(itemSize, itemSize);
    } else {
        float itemSize = self.mainCollectionView.frame.size.width / 2;
        return CGSizeMake(itemSize, itemSize);
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [SVProgressHUD showWithStatus:@"Loading image..."];
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    
    // Get current object from main imgs array
    PhotoEntity *entity = self.photosArray[indexPath.row];
    [manager downloadImageWithURL:[NSURL URLWithString:entity.urlOriginal] options:SDWebImageRetryFailed progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        [SVProgressHUD dismiss];
        CLImageEditor *imgVC = [[CLImageEditor alloc] initWithImage:image];
        imgVC.delegate = self;
        imgVC.title = @"Save to gallery";
        imgVC.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"default-bg.jpg"]];
        [self presentViewController:imgVC animated:YES completion:^{
            [[SDImageCache sharedImageCache] removeImageForKey:entity.urlOriginal fromDisk:YES];
        }];
    }];
}

#pragma mark - Private methods

- (void)fetchPhotoToCollectionView {
    NSArray *people = [[DatabaseHelper sharedInstance] getRandomPeople];
    self.photosArray = [[[DatabaseHelper sharedInstance] getRandomImagesFromPeopleArray:people] mutableCopy];
    
    [self.mainCollectionView reloadData];
    
	[SVProgressHUD dismiss];
}

@end
