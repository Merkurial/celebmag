//
//  ExploreViewController.m
//  CelebMag
//
//  Created by Dung Nguyen on 1/15/15.
//  Copyright (c) 2015 minhnh. All rights reserved.
//

#import "PhotosViewController.h"
#import "ThumbnailPhotoCollectionViewCell.h"
#import <UIImageView+WebCache.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

@interface PhotosViewController ()

@end

@implementation PhotosViewController {
	BOOL _isContentsHiding;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	[self.firstNameButton setTitle:@"" forState:UIControlStateNormal];
	[self.lastNameButton setTitle:@"" forState:UIControlStateNormal];
}

- (void)viewWillLayoutSubviews {
	UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)self.mainCollectionView.collectionViewLayout;
	if (!CGSizeEqualToSize(layout.itemSize, self.mainCollectionView.bounds.size)) {
		layout.itemSize = self.mainCollectionView.bounds.size;
	}
}

#pragma mark - UICollectionViewDataSource methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
	return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
	if (collectionView == self.thumbnailCollectionViewBot) {
		return self.photosForThumbnailBot.count;
	}
	else if (collectionView == self.thumbnailCollectionViewTop) {
		return self.photosForThumbnailTop.count;
	}
	else return self.photosForMainCollection.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
	if (collectionView == self.mainCollectionView) {
		PhotoEntity *photo;

		// Main = top + bot. Calculate the location of cell from indexPath
		if (indexPath.row < self.photosForThumbnailTop.count) {
			photo = self.photosForThumbnailTop[indexPath.row];
		}
		else {
			NSUInteger indexBot = indexPath.row - self.photosForThumbnailTop.count;
			photo = self.photosForThumbnailBot[indexBot];
		}

		MainPhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MainCell" forIndexPath:indexPath];
		cell.mainCellImageView.image = nil;
		cell.mainCellScrollView.scrollEnabled = NO;
		cell.mainCellScrollView.bouncesZoom = NO;
		cell.delegate = self;
		
		[cell.loadingIndicator startAnimating];

		[cell.mainCellImageView sd_setImageWithURL:[NSURL URLWithString:photo.urlMedium] completed: ^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
		    cell.mainCellScrollView.scrollEnabled = YES;

		    const CGSize viewSize = [UIScreen mainScreen].bounds.size;
		    const CGFloat minScale = viewSize.width / image.size.width;
		    NSLog(@"%@", NSStringFromCGSize(image.size));
		    NSLog(@"Medium: %@", photo.urlMedium);
			NSLog(@"Thumb: %@", photo.urlThumbnail);

		    [cell setMinZoomScaleWithScale:minScale];

		    [cell setScrollViewZoomScaleWithScale:1];
		    cell.mainCellImageView.frame = CGRectMake(0, 0, image.size.width, image.size.height);
		    cell.mainCellImageView.image = image;
		    cell.mainCellScrollView.contentSize = image.size;

		    [cell setScrollViewZoomScaleWithScale:cell.mainCellScrollView.minimumZoomScale];
		    cell.mainCellScrollView.contentOffset = CGPointMake(0, 0);

		    [cell.loadingIndicator stopAnimating];
		}];

		cell.photoEntity = photo;

		return cell;
	}
	else if (collectionView == self.thumbnailCollectionViewBot) {
		PhotoEntity *photo = self.photosForThumbnailBot[indexPath.row];
		ThumbnailPhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ThumbnailCell" forIndexPath:indexPath];
		cell.thumbnailCellImageView.image = nil;
		cell.imgBannerNew.hidden = YES;
		[cell.thumbnailCellImageView sd_setImageWithURL:[NSURL URLWithString:photo.urlThumbnail] placeholderImage:[UIImage imageNamed:@"loading.jpg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
			if (photo.isNew) {
				cell.imgBannerNew.hidden = NO;
			}
		}];

		return cell;
	}
	else if (collectionView == self.thumbnailCollectionViewTop) {
		PhotoEntity *photo = self.photosForThumbnailTop[indexPath.row];
		ThumbnailPhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ThumbnailCell" forIndexPath:indexPath];
		cell.thumbnailCellImageView.image = nil;
		cell.imgBannerNew.hidden = YES;
		[cell.thumbnailCellImageView sd_setImageWithURL:[NSURL URLWithString:photo.urlThumbnail] placeholderImage:[UIImage imageNamed:@"loading.jpg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
			if (photo.isNew) {
				cell.imgBannerNew.hidden = NO;
			}
		}];

		return cell;
	}

	return nil;
}

#pragma mark - UICollectionViewDelegate methods

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
	if (collectionView == self.mainCollectionView) {
		return CGSizeMake(CGRectGetWidth(self.mainCollectionView.frame), CGRectGetHeight(self.mainCollectionView.frame));
	} else {
		CGRect frame = self.thumbnailCollectionViewBot.frame;
		return CGSizeMake(frame.size.height * 0.725, frame.size.height);
	}
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
	if (collectionView == self.thumbnailCollectionViewTop) {
		[self.mainCollectionView scrollToItemAtIndexPath:indexPath
		                                atScrollPosition:UICollectionViewScrollPositionNone
		                                        animated:YES];
		PhotoEntity *entity = [self.photosForThumbnailTop objectAtIndex:indexPath.row];
		[self.firstNameButton setTitle:entity.person.firstName forState:UIControlStateNormal];
		[self.lastNameButton setTitle:entity.person.lastName forState:UIControlStateNormal];
		[[DatabaseHelper sharedInstance] updateFlagToBeRead:entity];
	}
	else if (collectionView == self.thumbnailCollectionViewBot) {
		NSUInteger index = self.photosForThumbnailTop.count + indexPath.row;
		NSIndexPath *indexPathForMain = [NSIndexPath indexPathForRow:index inSection:0];
		[self.mainCollectionView scrollToItemAtIndexPath:indexPathForMain atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
		PhotoEntity *entity = [self.photosForThumbnailBot objectAtIndex:indexPath.row];
		[self.firstNameButton setTitle:entity.person.firstName forState:UIControlStateNormal];
		[self.lastNameButton setTitle:entity.person.lastName forState:UIControlStateNormal];
		[[DatabaseHelper sharedInstance] updateFlagToBeRead:entity];
	}
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
	if (!decelerate) {
		if (scrollView == self.mainCollectionView) {
			MainPhotoCollectionViewCell *currentCell = [[self.mainCollectionView visibleCells] lastObject];
			[self.firstNameButton setTitle:currentCell.photoEntity.person.firstName forState:UIControlStateNormal];
			[self.lastNameButton setTitle:currentCell.photoEntity.person.lastName forState:UIControlStateNormal];
			[[DatabaseHelper sharedInstance] updateFlagToBeRead:currentCell.photoEntity];
		}
	}
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
	if (scrollView == self.mainCollectionView) {
		MainPhotoCollectionViewCell *currentCell = [[self.mainCollectionView visibleCells] lastObject];
		[self.firstNameButton setTitle:currentCell.photoEntity.person.firstName forState:UIControlStateNormal];
		[self.lastNameButton setTitle:currentCell.photoEntity.person.lastName forState:UIControlStateNormal];
		[[DatabaseHelper sharedInstance] updateFlagToBeRead:currentCell.photoEntity];
	}
}

#pragma mark - MainPhotoCollectionViewCellDelegate methods/Actions

- (IBAction)shareFbButtonTapped:(id)sender {
	if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fb://"]]) {
		FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
		MainPhotoCollectionViewCell *currentCell = [[self.mainCollectionView visibleCells] lastObject];
		content.imageURL = [NSURL URLWithString:currentCell.photoEntity.urlOriginal];
		content.contentURL = [NSURL URLWithString:@"https://itunes.apple.com/us/app/wallpapers-beauty/id1020936716?ls=1&mt=8"];
		content.contentTitle = @"Beautiful photo from Wallpapers Beauty";
		content.contentDescription = @"This image is generated from Wallpapers Beauty app, available from the AppStore";
		[FBSDKShareDialog showFromViewController:self withContent:content delegate:nil];
	} else {
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Facebook not installed" message:@"Please install Facebook from the App Store" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alertView show];
	}
}

- (void)mainPhotoCollectionViewCellSelectedName:(MainPhotoCollectionViewCell *)cell {
}

- (void)mainPhotoCollectionViewCellTapScrollView:(MainPhotoCollectionViewCell *)cell {
	_isContentsHiding = !_isContentsHiding;
	if (_isContentsHiding == YES) {
		[UIView animateWithDuration:0.2 animations: ^{
		    cell.bioLabel.alpha = 0;
			cell.intoAlbumButton.alpha = 0;
//			cell.adBannerView.alpha = 0;

		    self.thumbnailCollectionViewBot.alpha = 0;
		    self.thumbnailCollectionViewTop.alpha = 0;
			self.firstNameButton.alpha = 0;
			self.lastNameButton.alpha = 0;
			self.gotoAlbumButton.alpha = 0;
			self.shareFbButton.alpha = 0;
		}];
		self.mainCollectionView.scrollEnabled = NO;
	}
	else {
		[UIView animateWithDuration:0.2 animations: ^{
		    cell.bioLabel.alpha = 1;
			cell.intoAlbumButton.alpha = 1;
//			cell.adBannerView.alpha = 1;

		    self.thumbnailCollectionViewBot.alpha = 1;
		    self.thumbnailCollectionViewTop.alpha = 1;
			self.firstNameButton.alpha = 1;
			self.lastNameButton.alpha = 1;
			self.gotoAlbumButton.alpha = 1;
			self.shareFbButton.alpha = 1;
		}];
		self.mainCollectionView.scrollEnabled = YES;
	}
}

@end
