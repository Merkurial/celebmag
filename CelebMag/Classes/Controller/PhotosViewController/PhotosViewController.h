//
//  ExploreViewController.h
//  CelebMag
//
//  Created by Dung Nguyen on 1/15/15.
//  Copyright (c) 2015 minhnh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DatabaseHelper.h"
#import "PersonEntity.h"
#import "PhotoEntity.h"
#import "Constant.h"
#import "MainPhotoCollectionViewCell.h"

@interface PhotosViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, MainPhotoCollectionViewCellDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *mainCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *thumbnailCollectionViewBot;
@property (weak, nonatomic) IBOutlet UICollectionView *thumbnailCollectionViewTop;
@property (weak, nonatomic) IBOutlet UIButton *firstNameButton;
@property (weak, nonatomic) IBOutlet UIButton *lastNameButton;
@property (weak, nonatomic) IBOutlet UIButton *gotoAlbumButton;
@property (weak, nonatomic) IBOutlet UIButton *shareFbButton;

@property (strong, nonatomic) NSArray *photosForThumbnailBot;
@property (strong, nonatomic) NSArray *photosForThumbnailTop;
@property (strong, nonatomic) NSArray *photosForMainCollection;

@end
