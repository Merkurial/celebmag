//
//  PersonViewController.m
//  CelebMag
//
//  Created by Dung Nguyen on 1/16/15.
//  Copyright (c) 2015 minhnh. All rights reserved.
//

#import "PersonViewController.h"
#import "ImageCropView.h"
#import "PersonEntity.h"
#import <UIImageView+WebCache.h>
#import <SVProgressHUD.h>
#import <CLImageEditor/CLImageEditor.h>

@interface PersonViewController () <CLImageEditorDelegate, UIAlertViewDelegate>

@end

@implementation PersonViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	[self configureBarButtons];

	self.photosForThumbnailBot = [[DatabaseHelper sharedInstance] getRandomImagesOfPerson:self.person];
	NSArray *arrayForMain = [[NSArray alloc] initWithArray:self.photosForThumbnailBot];
	self.photosForMainCollection = arrayForMain;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
	PhotoEntity *photo = self.photosForThumbnailBot[indexPath.row];
	UICollectionViewCell *cell = [super collectionView:collectionView cellForItemAtIndexPath:indexPath];
	if (collectionView == self.mainCollectionView) {
		if ([photo.person.biography isEqualToString:@""]) {
			((MainPhotoCollectionViewCell *)cell).bioLabel.text = @"Model's biography hasn't been updated yet.";
		}
		else {
			((MainPhotoCollectionViewCell *)cell).bioLabel.text = [NSString stringWithFormat:@"Source: %@", photo.person.biography];
			
		}
		
//		MainPhotoCollectionViewCell *mainCell = (MainPhotoCollectionViewCell *)cell;
//		mainCell.adBannerView.adUnitID = @"ca-app-pub-8195778844781144/1784967519";
//		mainCell.adBannerView.rootViewController = self;
//		GADRequest *request = [GADRequest request];
//		request.gender = kGADGenderMale;
//		request.testDevices = @[kGADSimulatorID];
//		[mainCell.adBannerView loadRequest:request];
	}
	return cell;
}

- (void)viewDidLayoutSubviews {
	if (self.photoToScrollTo != nil) {
		int row = 0;
		
		for (PhotoEntity *entity in self.photosForMainCollection) {
			if ([entity.photoId isEqualToString:self.photoToScrollTo.photoId]) {
				break;
			}
			
			row += 1;
		}
		
		NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
		[self.mainCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
		[self.thumbnailCollectionViewBot scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
	}
}

#pragma mark - Private method

- (void)configureBarButtons {
	UIBarButtonItem *saveToGalleryBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"save2gallery.png"] style:UIBarButtonItemStylePlain target:self action:@selector(saveToGalleryButtonTapped)];
	NSArray *arrayBarButtons = @[saveToGalleryBarButton];
    
	self.navigationItem.rightBarButtonItems = arrayBarButtons;
}

- (void)saveToGalleryButtonTapped {
	MainPhotoCollectionViewCell *currentCell = [[self.mainCollectionView visibleCells] lastObject];
	NSLog(@"Number of visible cell: %lu", (unsigned long)[[self.mainCollectionView visibleCells] count]);
	if (currentCell.mainCellImageView.image != nil) {
		[SVProgressHUD showWithStatus:@"Loading image..."];
		SDWebImageManager *manager = [SDWebImageManager sharedManager];
		
		// Get current object from main imgs array
		PhotoEntity *entity = [self.photosForMainCollection objectAtIndex:[self.mainCollectionView indexPathForCell:currentCell].row];
		[manager downloadImageWithURL:[NSURL URLWithString:entity.urlOriginal] options:SDWebImageRetryFailed progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
			[SVProgressHUD dismiss];
			CLImageEditor *imgVC = [[CLImageEditor alloc] initWithImage:image];
			imgVC.delegate = self;
			imgVC.title = @"Save to gallery";
			imgVC.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"default-bg.jpg"]];
			[self.navigationController presentViewController:imgVC animated:YES completion:nil];
		}];
	}
	else {
		UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"Missing image" message:@"Please wait for the image to load successfully" preferredStyle:UIAlertControllerStyleAlert];
		[alertVC addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
		[self.navigationController presentViewController:alertVC animated:YES completion:nil];
	}
}

- (void)imageEditor:(CLImageEditor *)editor didFinishEdittingWithImage:(UIImage *)image {
    [SVProgressHUD showWithStatus:@"Saving image"];
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
}

- (void)imageEditorDidCancel:(CLImageEditor *)editor {
    //	[self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"Unable to save image to Photo Album."
                                                       delegate:self cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
    } else {
        [SVProgressHUD dismiss];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Successfully" message:@"Saved image successfully. Please switch to your Photos/Gallery app to set it as your homescreen/lockscreen." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
