//
//  PersonViewController.h
//  CelebMag
//
//  Created by Dung Nguyen on 1/16/15.
//  Copyright (c) 2015 minhnh. All rights reserved.
//

#import "PhotosViewController.h"

@interface PersonViewController : PhotosViewController

@property (strong, nonatomic) PersonEntity *person;
@property (strong, nonatomic) PhotoEntity *photoToScrollTo;

@end
