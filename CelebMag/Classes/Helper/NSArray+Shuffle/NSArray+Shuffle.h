//
//  NSArray+Shuffle.h
//  CelebMag
//
//  Created by Dung Nguyen on 1/16/15.
//  Copyright (c) 2015 minhnh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Shuffle)

- (NSArray *)shuffledArray;

@end
