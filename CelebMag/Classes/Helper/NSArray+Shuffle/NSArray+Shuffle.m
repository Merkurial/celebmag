//
//  NSArray+Shuffle.m
//  CelebMag
//
//  Created by Dung Nguyen on 1/16/15.
//  Copyright (c) 2015 minhnh. All rights reserved.
//

#import "NSArray+Shuffle.h"

@implementation NSArray (Shuffle)

- (NSArray *)shuffledArray {
    NSMutableArray *shuffedArray = [self mutableCopy];
    NSUInteger count = [self count];
    for (NSUInteger i = 0; i < count; ++i) {
        NSInteger remainingCount = count - i;
        NSInteger exchangeIndex = i + arc4random_uniform((u_int32_t )remainingCount);
        [shuffedArray exchangeObjectAtIndex:i withObjectAtIndex:exchangeIndex];
    }
    return shuffedArray;
}

@end
