//
//  ThumbnailPhotoCollectionViewCell.h
//  CelebMag
//
//  Created by Dung Nguyen on 1/16/15.
//  Copyright (c) 2015 minhnh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThumbnailPhotoCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *thumbnailCellImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *thumbImgLeadConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *imgBannerNew;

@end
