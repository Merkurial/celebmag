//
//  PhotoCollectionViewCell.m
//  CelebMag
//
//  Created by Dung Nguyen on 1/16/15.
//  Copyright (c) 2015 minhnh. All rights reserved.
//

#import "MainPhotoCollectionViewCell.h"

@implementation MainPhotoCollectionViewCell

#pragma mark - Init

- (void)awakeFromNib {
	self.mainCellScrollView.delegate = self;
	self.mainCellScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	self.mainCellScrollView.autoresizesSubviews = NO;
	self.mainCellScrollView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"default-bg.jpg"]];
	self.mainCellScrollView.showsHorizontalScrollIndicator = NO;
	self.mainCellScrollView.showsVerticalScrollIndicator = NO;

	self.mainCellScrollView.maximumZoomScale = 1.75;
	if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
		self.mainCellScrollView.minimumZoomScale = 0.5;
	}
	else {
		self.mainCellScrollView.minimumZoomScale = 0.25;
		self.bioLabel.font = [UIFont systemFontOfSize:12];
	}
//	NSLog(@"Scroll view inside cell frame: %@", NSStringFromCGRect(self.mainCellScrollView.frame));

	UITapGestureRecognizer *hideAllGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedScrollView:)];
	hideAllGesture.numberOfTapsRequired = 1;
	[self.mainCellScrollView addGestureRecognizer:hideAllGesture];

	UITapGestureRecognizer *zoomGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTappedScrollView:)];
	zoomGesture.numberOfTapsRequired = 2;
	[self.mainCellScrollView addGestureRecognizer:zoomGesture];

	self.mainCellImageView = [[UIImageView alloc] initWithFrame:self.mainCellScrollView.bounds];
//	NSLog(@"Image view init frame: %@", NSStringFromCGRect(self.mainCellImageView.frame));
	self.mainCellImageView.contentMode = UIViewContentModeScaleAspectFill;
	self.mainCellImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	self.mainCellImageView.clipsToBounds = YES;
	[self.mainCellScrollView addSubview:self.mainCellImageView];

	self.loadingIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(CGRectGetWidth([UIScreen mainScreen].bounds) / 2 - 30, CGRectGetHeight([UIScreen mainScreen].bounds) / 2 - 30, 40, 40)];
	self.loadingIndicator.hidesWhenStopped = YES;
	self.loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
	[self addSubview:self.loadingIndicator];
}

#pragma mark - Delegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
	return self.mainCellImageView;
}

#pragma mark - Action

- (IBAction)nameSelected:(id)sender {
	[self.delegate mainPhotoCollectionViewCellSelectedName:self];
}

- (void)tappedScrollView:(id)sender {
//	NSLog(@"Tapped");
	[self.delegate mainPhotoCollectionViewCellTapScrollView:self];
}

- (void)doubleTappedScrollView:(UITapGestureRecognizer *)sender {
	const CGPoint imagePoint = [sender locationInView:sender.view];
	CGFloat minimumZoomScale = self.mainCellScrollView.minimumZoomScale;
	if (self.mainCellScrollView.zoomScale > minimumZoomScale) { // Zoom out
		[self.mainCellScrollView setZoomScale:minimumZoomScale animated:YES];
	}
	else { // Zoom in
		const CGFloat maximumZoomScale = self.mainCellScrollView.maximumZoomScale;
		const CGFloat newZoomScale = MIN(minimumZoomScale * 2, maximumZoomScale);
		const CGFloat width = self.bounds.size.width / newZoomScale;
		const CGFloat height = self.bounds.size.height / newZoomScale;
		const CGRect zoomRect = CGRectMake(imagePoint.x, imagePoint.x, width, height);
		[self.mainCellScrollView zoomToRect:zoomRect animated:YES];
	}
}

- (void)setScrollViewZoomScaleWithScale:(CGFloat)scale {
	self.mainCellScrollView.zoomScale = scale;
}

- (void)setMinZoomScaleWithScale:(CGFloat)scale {
	self.mainCellScrollView.minimumZoomScale = scale;
}

@end
