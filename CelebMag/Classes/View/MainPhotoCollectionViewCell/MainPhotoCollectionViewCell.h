//
//  PhotoCollectionViewCell.h
//  CelebMag
//
//  Created by Dung Nguyen on 1/16/15.
//  Copyright (c) 2015 minhnh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoEntity.h"
#import "CenteringScrollView.h"

@protocol MainPhotoCollectionViewCellDelegate;

@interface MainPhotoCollectionViewCell : UICollectionViewCell <UIScrollViewDelegate>

@property (strong, nonatomic) PhotoEntity *photoEntity;
@property (strong, nonatomic) UIImageView *mainCellImageView;
@property (strong, nonatomic) UIActivityIndicatorView *loadingIndicator;
@property (weak, nonatomic) IBOutlet CenteringScrollView *mainCellScrollView;
@property (weak, nonatomic) IBOutlet UILabel *bioLabel;
@property (weak, nonatomic) IBOutlet UIButton *intoAlbumButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintFirstName;

@property (weak, nonatomic) id <MainPhotoCollectionViewCellDelegate> delegate;

- (void)setScrollViewZoomScaleWithScale:(CGFloat)scale;
- (void)setMinZoomScaleWithScale:(CGFloat)scale;

@end

@protocol MainPhotoCollectionViewCellDelegate <NSObject>

- (void)mainPhotoCollectionViewCellSelectedName:(MainPhotoCollectionViewCell *)cell;
- (void)mainPhotoCollectionViewCellTapScrollView:(MainPhotoCollectionViewCell *)cell;

@end
