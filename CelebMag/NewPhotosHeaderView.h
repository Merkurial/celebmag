//
//  NewPhotosHeaderView.h
//  CelebMag
//
//  Created by minhnh on 12/29/15.
//  Copyright © 2015 minhnh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewPhotosHeaderView : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UILabel *personLabel;

@end
