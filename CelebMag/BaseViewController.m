//
//  BaseViewController.m
//  CelebMag
//
//  Created by minhnh on 5/13/16.
//  Copyright © 2016 minhnh. All rights reserved.
//

#import "BaseViewController.h"
#import <UIViewController+MMDrawerController.h>
#import <MMDrawerBarButtonItem.h>

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self settingDrawerOpenButton];
}

- (void)settingDrawerOpenButton {
    MMDrawerBarButtonItem *leftButton = [[MMDrawerBarButtonItem alloc]
                                         initWithTarget:self
                                         action:@selector(selectOpenDrawerButton:)];
    self.navigationItem.leftBarButtonItem = leftButton;
}

- (void)selectOpenDrawerButton:(id)sender {
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft
                                      animated:YES
                                    completion:nil];
}

@end
