//
//  NewUpdateViewController.m
//  CelebMag
//
//  Created by minhnh on 11/13/15.
//  Copyright © 2015 minhnh. All rights reserved.
//

#import "NewUpdateViewController.h"
#import "PersonViewController.h"
#import "DatabaseHelper.h"
#import "NewUpdatedViewCell.h"
#import "NewPhotosHeaderView.h"
#import "PhotoEntity.h"
#import "PersonEntity.h"
#import <SVProgressHUD.h>
#import <UIImageView+WebCache.h>
#import <UIViewController+MMDrawerController.h>
#import <MMDrawerBarButtonItem.h>
#import <CLImageEditor.h>

@interface NewUpdateViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, CLImageEditorDelegate, UIAlertViewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *numOfNewPicsLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *updatedPhotosCollectionView;
@property (strong, nonatomic) UITextField *searchTextField;

@property (strong, nonatomic) NSArray *peopleArray;

@end

@implementation NewUpdateViewController {
	NSMutableArray *arrayNewPhotos;
	UIView *currentTitleView;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.
    
	self.updatedPhotosCollectionView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"default-bg.jpg"]];
	self.numOfNewPicsLabel.text = @"";
	
	// Init search tf
	self.searchTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
	self.searchTextField.backgroundColor = [UIColor whiteColor];
	self.searchTextField.layer.cornerRadius = 5;
	self.searchTextField.layer.masksToBounds = YES;
	self.searchTextField.clearButtonMode = UITextFieldViewModeAlways;
	self.searchTextField.autocorrectionType = UITextAutocorrectionTypeNo;
	[self.searchTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
	self.searchTextField.delegate = self;
	self.searchTextField.hidden = YES;
	currentTitleView = self.navigationItem.titleView;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self fetchNewPhotos];
    [self.updatedPhotosCollectionView reloadData];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (void)fetchNewPhotos {
    self.peopleArray = [[DatabaseHelper sharedInstance] getPeopleThatHaveUpdatedPhotosAndNameContain:@""];
    arrayNewPhotos = [[NSMutableArray alloc] init];
    [self initArrayNewPhotos];
    self.numOfNewPicsLabel.text = [NSString stringWithFormat:@"Total %lu new photos", [[[DatabaseHelper sharedInstance] getNewUpdatedPhotos] count]];
}

#pragma mark - Init data

- (void)initArrayNewPhotos {
	[arrayNewPhotos removeAllObjects];
	
	for (PersonEntity *entity in self.peopleArray) {
		NSArray *array = [[DatabaseHelper sharedInstance] getNewPhotoForPerson:entity];
		[arrayNewPhotos addObject:array];
	}
}

#pragma mark - UICollectionView datasource/delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
	return [self.peopleArray count];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
	if (kind == UICollectionElementKindSectionHeader) {
		NewPhotosHeaderView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"NewViewHeader" forIndexPath:indexPath];
		PersonEntity *pEntity = [self.peopleArray objectAtIndex:indexPath.section];
		header.personLabel.text = pEntity.name;
        header.personLabel.textColor = [UIColor whiteColor];
		
		return header;
	}
	
	return nil;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
	NSArray *array = [arrayNewPhotos objectAtIndex:section];
	return [array count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
	NewUpdatedViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NewUpdatedCell" forIndexPath:indexPath];
	
	if (cell) {
		PhotoEntity *entity = (PhotoEntity *)[[arrayNewPhotos objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
		if (cell.previewImageView.image == nil) {
            cell.imageLoader.lineWidth = 5;
            cell.imageLoader.color = [UIColor colorWithRed:0.133 green:0.663 blue:0.894 alpha:1.000];
			[cell.imageLoader startAnimation];
		}
		[cell.previewImageView sd_setImageWithURL:[NSURL URLWithString:entity.urlThumbnail] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
			cell.previewImageView.image = image;
            [cell.imageLoader stopAnimation];
            [cell.imageLoader removeFromSuperview];
		}];
	}
	
	return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
	int lengthItem = [UIScreen mainScreen].bounds.size.width / 4;
	return CGSizeMake(lengthItem, lengthItem);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
	return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
	return 0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
	return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    // Mark as read
    PhotoEntity *entity = arrayNewPhotos[indexPath.section][indexPath.row];
    [[DatabaseHelper sharedInstance] updateFlagToBeRead:entity];
}

#pragma mark - Action

- (IBAction)searchButtonTapped:(UIBarButtonItem *)sender {
	self.searchTextField.hidden = !self.searchTextField.hidden;
	self.navigationItem.titleView = self.searchTextField.hidden ? currentTitleView : self.searchTextField;
	[self.searchTextField becomeFirstResponder];
}

- (void)textFieldDidChange:(UITextField *)textField {
	self.peopleArray = [[DatabaseHelper sharedInstance] getPeopleThatHaveUpdatedPhotosAndNameContain:textField.text];
	[self initArrayNewPhotos];
	[self.updatedPhotosCollectionView reloadData];
	[self.updatedPhotosCollectionView setContentOffset:CGPointMake(0, 0) animated:YES];
}

#pragma mark - Delegates

- (void)imageEditor:(CLImageEditor *)editor didFinishEdittingWithImage:(UIImage *)image {
	[SVProgressHUD showWithStatus:@"Saving image"];
	UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
}

- (void)imageEditorDidCancel:(CLImageEditor *)editor {
	//	[self.navigationController popViewControllerAnimated:YES];
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
	if (error) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
														message:@"Unable to save image to Photo Album."
													   delegate:self cancelButtonTitle:@"Ok"
											  otherButtonTitles:nil];
		[alert show];
	} else {
		[SVProgressHUD dismiss];
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Successfully" message:@"Saved image successfully. Please switch to your Photos/Gallery app to set it as your homescreen/lockscreen." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
	}
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	self.searchTextField.hidden = YES;
	self.navigationItem.titleView = currentTitleView;
	[self textFieldDidChange:textField];
	return YES;
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"NewShowPerson"]) {
		PersonViewController *pvc = (PersonViewController *)segue.destinationViewController;
		NSIndexPath *index = [[self.updatedPhotosCollectionView indexPathsForSelectedItems] firstObject];
		
		pvc.person = [self.peopleArray objectAtIndex:index.section];
        pvc.title = pvc.person.name;
		pvc.photoToScrollTo = [[arrayNewPhotos objectAtIndex:index.section] objectAtIndex:index.row];
	}
}

@end
