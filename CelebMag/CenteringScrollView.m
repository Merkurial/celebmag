//
//  CenteringScrollView.m
//  CelebMag
//
//  Created by minhnh on 4/10/15.
//  Copyright (c) 2015 minhnh. All rights reserved.
//

#import "CenteringScrollView.h"

@implementation CenteringScrollView

- (void)setContentOffset:(CGPoint)contentOffset
{
	CGSize contentSize = self.contentSize;
	CGSize scrollViewSize = self.bounds.size;
	
	if (contentSize.width < scrollViewSize.width)
	{
		contentOffset.x = -(scrollViewSize.width - contentSize.width) / 2.0;
	}
	
	if (contentSize.height < scrollViewSize.height)
	{
		contentOffset.y = -(scrollViewSize.height - contentSize.height) / 2.0;
	}
	
	[super setContentOffset:contentOffset];
}

@end
