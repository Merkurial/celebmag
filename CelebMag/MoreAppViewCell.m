//
//  MoreAppViewCell.m
//  CelebMag
//
//  Created by minhnh on 7/4/15.
//  Copyright (c) 2015 minhnh. All rights reserved.
//

#import "MoreAppViewCell.h"

@implementation MoreAppViewCell

- (void)awakeFromNib {
    // Initialization code
	self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"default-bg.jpg"]];
	self.appLabelName.textColor = [UIColor whiteColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
