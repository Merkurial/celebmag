//
//  PersonCategoryViewController.m
//  CelebMag
//
//  Created by minhnh on 5/13/16.
//  Copyright © 2016 minhnh. All rights reserved.
//

#import "PersonCategoryViewController.h"
#import "PersonViewController.h"

#import "DatabaseHelper.h"
#import "PersonEntity.h"

#import <UIImage+SVG.h>
#import "PersonCategoryCell.h"

@implementation PersonCategoryViewController {
    NSArray *allPersonArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"default-bg.jpg"]];
    [self fetchDataToPersonArray];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchDataWithNotification:) name:@"PersonCategoryTableViewWillReload" object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Table view datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [allPersonArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PersonCategoryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PersonCategoryCell" forIndexPath:indexPath];
    
    if (cell) {
        cell.folderImageView.image = [UIImage imageWithSVGNamed:@"category-icon" targetSize:cell.folderImageView.frame.size fillColor:[UIColor whiteColor]];
        cell.personCategoryNameLabel.text = [self stringForCellAtIndexPath:indexPath];
        cell.personCategoryNameLabel.textColor = [UIColor colorWithWhite:0.902 alpha:1.000];
        cell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"default-bg.jpg"]];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    
    return screenSize.height / 11;
}

#pragma mark - Privates

- (void)fetchDataToPersonArray {
    allPersonArray = [[DatabaseHelper sharedInstance] getSortedPeople];
    [self.tableView reloadData];
}

- (void)fetchDataWithNotification:(NSNotification *)notification {
    allPersonArray = [[DatabaseHelper sharedInstance] getSortedPeople];
    [self.tableView reloadData];
}

- (NSString *)stringForCellAtIndexPath:(NSIndexPath *)indexPath {
    PersonEntity *entity = allPersonArray[indexPath.row];
    return [NSString stringWithFormat:@"%@ (%ld)", entity.name, (long)[[DatabaseHelper sharedInstance] getTotalNumOfPhotosOfPerson:entity]];
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqual: @"ShowPerson"]) {
        PersonViewController *pvc = (PersonViewController* )segue.destinationViewController;
        
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        pvc.person = allPersonArray[indexPath.row];
        pvc.title = [(PersonCategoryCell *)sender personCategoryNameLabel].text;
    }
}

@end
