//
//  RootDrawerController.m
//  CelebMag
//
//  Created by minhnh on 4/3/15.
//  Copyright (c) 2015 minhnh. All rights reserved.
//

#import "RootDrawerController.h"
#import <UIViewController+MMDrawerController.h>

@interface RootDrawerController ()

@end

@implementation RootDrawerController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	self.closeDrawerGestureModeMask = MMCloseDrawerGestureModeTapCenterView;
	self.centerHiddenInteractionMode = MMDrawerOpenCenterInteractionModeNavigationBarOnly;
	UIViewController *centerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PAGE_VC"];
	[self setCenterViewController:centerVC];
	
	UIViewController *leftVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SIDE_DRAWER_CONTROLLER"];
	[self setLeftDrawerViewController:leftVC];
	
	self.closeDrawerGestureModeMask = MMCloseDrawerGestureModePanningDrawerView;
}

@end
