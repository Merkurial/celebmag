//
//  DatabaseHelper.h
//  GenerateDB
//
//  Created by minhnh on 11/8/14.
//  Copyright (c) 2014 minhnh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDB.h"

#define IMG_NAME @"filename"
#define IMG_PERSON_ID @"person_id"
#define NUM_OF_RND_IMGS 3

@class PersonEntity;
@class PhotoEntity;

@protocol FinishUpdatingDelegate <NSObject>

- (void)finishingUpdateWorkAndNeedToDownload:(BOOL)isNeedToDownload;
- (void)didFailedToUpdate;

@end

@interface DatabaseHelper : NSObject

+ (DatabaseHelper *)sharedInstance;

- (void)checkUpdateWithCompletionHandler:(id<FinishUpdatingDelegate>)delegate;

- (NSArray *)getNewUpdatedPhotos;
- (NSArray *)getNewPhotoForPerson:(PersonEntity *)personEntity;

- (NSArray *)getRandomPeople;
- (NSArray *)getSortedPeople;
- (NSInteger)getTotalNumOfPhotosOfPerson:(PersonEntity *)person;
- (NSArray *)getPeopleThatHaveUpdatedPhotosAndNameContain:(NSString *)name;
- (PersonEntity *)getPersonEntityFromPhotoEntity:(PhotoEntity *)photoEntity;
- (NSArray *)getPeopleListFromGivenName:(NSString *)name;

- (NSArray *)getRandomImagesFromPeopleArray:(NSArray *)peopleArray;
- (NSArray *)getRandomImagesOfPerson:(PersonEntity *)person;
- (PhotoEntity *)getPhotoBasedOnPerson:(PersonEntity *)person;

- (NSArray *)getOccupationBasedOnPerson:(PersonEntity *)person;

- (void)updateFlagToBeRead:(PhotoEntity *)entity;

@end
