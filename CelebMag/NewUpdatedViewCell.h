//
//  NewUpdatedViewCell.h
//  CelebMag
//
//  Created by minhnh on 11/13/15.
//  Copyright © 2015 minhnh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BLLoader.h"

@interface NewUpdatedViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *previewImageView;
@property (weak, nonatomic) IBOutlet BLLoader *imageLoader;

@end
