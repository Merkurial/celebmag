//
//  ExplorePhotoCell.h
//  CelebMag
//
//  Created by minhnh on 4/10/16.
//  Copyright © 2016 minhnh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BLLoader.h"

@interface ExplorePhotoCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *categoryName;
@property (weak, nonatomic) IBOutlet UIImageView *mainImage;
@property (weak, nonatomic) IBOutlet UIImageView *downloadIconImage;
@property (weak, nonatomic) IBOutlet UILabel *numDownloadLabel;
@property (weak, nonatomic) IBOutlet BLLoader *imageLoader;

@end
