//
//  DrawMenuCell.h
//  CelebMag
//
//  Created by minhnh on 11/6/15.
//  Copyright © 2015 minhnh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DrawMenuCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *menuImage;
@property (weak, nonatomic) IBOutlet UILabel *menuLabel;

@end
