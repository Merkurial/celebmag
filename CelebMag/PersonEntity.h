//
//  PersonEntity.h
//  CelebMag
//
//  Created by minhnh on 12/5/14.
//  Copyright (c) 2014 minhnh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PersonEntity : NSObject

@property (nonatomic, assign) NSUInteger personId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *biography;
@property (nonatomic, readonly) NSString *firstName;
@property (nonatomic, readonly) NSString *lastName;

- (instancetype)initWithPersonId:(NSUInteger)personId name:(NSString *)name biography:(NSString *)biography;

@end
