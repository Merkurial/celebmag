//
//  AllPageViewController.m
//  CelebMag
//
//  Created by minhnh on 5/7/16.
//  Copyright © 2016 minhnh. All rights reserved.
//

#import "AllPageViewController.h"
#import "PersonViewController.h"
#import <MBXPageViewController/MBXPageViewController.h>
#import <UIViewController+MMDrawerController.h>

#import "DatabaseHelper.h"
#import "PersonEntity.h"
#import "PhotoEntity.h"

#import <MMDrawerBarButtonItem.h>
#import "MLPAutoCompleteTextField.h"
#import "MLPAutoCompleteTextFieldDelegate.h"
#import "MLPAutoCompleteTextFieldDataSource.h"
#import <UIImageView+WebCache.h>

@interface AllPageViewController () <MBXPageControllerDataSource, MBXPageControllerDataDelegate, MLPAutoCompleteTextFieldDelegate, MLPAutoCompleteTextFieldDataSource, UITextFieldDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UISegmentedControl *exploreCategoriesSegment;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (nonatomic, strong) MLPAutoCompleteTextField *searchTextField;

@end

@implementation AllPageViewController {
    // For search displaying purpose
    UIView *_darkOpaqueView;
    NSArray *_arrayPeopleFetchedFromSearch;
    NSArray *suggestPeopleArray;
}

static NSString *const ExploreShowPersonSegueIdentifier = @"ExploreShowPerson";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Generate view pager
    MBXPageViewController *pageVC = [MBXPageViewController new];
    
    pageVC.MBXDataSource = self;
    pageVC.MBXDataDelegate = self;
    pageVC.pageMode = MBX_SegmentController;
    [pageVC reloadPagesToCurrentPageIndex:0];
    
    // Search text field
    [self settingSearchTextField];
    
    // Segment control font
    NSDictionary *attr = [[NSDictionary alloc] initWithObjectsAndKeys:[UIFont fontWithName:@"AvenirNext-Regular" size:15], NSFontAttributeName, nil];
    [self.exploreCategoriesSegment setTitleTextAttributes:attr forState:UIControlStateNormal];
    
    // Back bar button
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleBordered target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
}

#pragma mark - Button actions

- (IBAction)searchClicked:(UIBarButtonItem *)sender {
    if (!_darkOpaqueView) {
        // Show text field
        _darkOpaqueView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _darkOpaqueView.backgroundColor = [UIColor blackColor];
        _darkOpaqueView.alpha = 0.7;
        UITapGestureRecognizer *gesture =
        [[UITapGestureRecognizer alloc] initWithTarget:self	action:@selector(endSearching:)];
        gesture.delegate = self;
        [_darkOpaqueView addGestureRecognizer:gesture];
        [_darkOpaqueView addSubview:self.searchTextField];
        
        [self.view addSubview:_darkOpaqueView];
        
        self.searchTextField.text = @"";
        [self.searchTextField becomeFirstResponder];
    } else {
        [self resignFirstResponder];
        [_darkOpaqueView removeFromSuperview];
        _darkOpaqueView = nil;
    }
}

#pragma mark - VC Datasource

- (NSArray *)MBXPageButtons {
    return @[self.exploreCategoriesSegment];
}

- (UIView *)MBXPageContainer {
    return self.containerView;
}

- (NSArray *)MBXPageControllers {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIViewController *exploreVC = [storyboard instantiateViewControllerWithIdentifier:@"EXPLORE_VC"];
    UIViewController *personCategoryVC = [storyboard instantiateViewControllerWithIdentifier:@"PERSON_CATEGORY_VC"];
    
    return @[exploreVC, personCategoryVC];
}

- (void)MBXPageChangedToIndex:(NSInteger)index {
    NSLog(@"%@ %ld", [self class], (long)index);
}

#pragma mark - Search text field Datasource

- (NSArray *)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
      possibleCompletionsForString:(NSString *)string {
    if ([string isEqualToString:@""]) {
        return @[];
    }
    
    NSMutableArray *arrayResult = [NSMutableArray new];
    suggestPeopleArray = [[DatabaseHelper sharedInstance] getPeopleListFromGivenName:string];
    
    for (PersonEntity *entity in suggestPeopleArray) {
        [arrayResult addObject:entity.name];
    }
    
    return [arrayResult copy];
}

- (BOOL)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
          shouldConfigureCell:(UITableViewCell *)cell
       withAutoCompleteString:(NSString *)autocompleteString
         withAttributedString:(NSAttributedString *)boldedString
        forAutoCompleteObject:(id <MLPAutoCompletionObject> )autocompleteObject
            forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PersonEntity *entity = [[[DatabaseHelper sharedInstance] getPeopleListFromGivenName:autocompleteString] firstObject];
    NSArray *occupationList = [[DatabaseHelper sharedInstance] getOccupationBasedOnPerson:entity];
    
    cell.indentationLevel = 1;
    cell.indentationWidth = 60.0f;
    
    cell.detailTextLabel.text = [occupationList componentsJoinedByString:@", "];
    PhotoEntity *photoEntity = [[DatabaseHelper sharedInstance] getPhotoBasedOnPerson:entity];
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 50, 50)];
    imgView.backgroundColor = [UIColor clearColor];
    imgView.layer.cornerRadius = 8.0f;
    imgView.layer.masksToBounds = YES;
    imgView.image = nil;
    [imgView sd_setImageWithURL:[NSURL URLWithString:photoEntity.urlThumbnail] completed: ^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        imgView.image = image;
        [cell.contentView addSubview:imgView];
    }];
    
    return YES;
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
  didSelectAutoCompleteString:(NSString *)selectedString
       withAutoCompleteObject:(id <MLPAutoCompletionObject> )selectedObject
            forRowAtIndexPath:(NSIndexPath *)indexPath {
    _arrayPeopleFetchedFromSearch = [[DatabaseHelper sharedInstance] getPeopleListFromGivenName:selectedString];
    PersonEntity *resultEntity = [_arrayPeopleFetchedFromSearch firstObject];
    [self performSegueWithIdentifier:ExploreShowPersonSegueIdentifier sender:resultEntity];

    self.searchTextField.text = @"";
    
    NSLog(@"I will call this for later");
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
       shouldReceiveTouch:(UITouch *)touch {
    if (touch.view != _darkOpaqueView) {
        return NO;
    }
    
    return YES;
}

#pragma mark - Textfield delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.searchTextField resignFirstResponder];
    [_darkOpaqueView removeFromSuperview];
    _darkOpaqueView = nil;
    
    if (![textField.text isEqualToString:@""]) {
        NSString *firstName = (NSString *)[self.searchTextField.autoCompleteSuggestions firstObject];
        PersonEntity *firstResult = (PersonEntity *)[[[DatabaseHelper sharedInstance] getPeopleListFromGivenName:firstName] firstObject];
        [self performSegueWithIdentifier:ExploreShowPersonSegueIdentifier sender:firstResult];
        
    }
    
    return YES;
}

#pragma mark - Privates

- (void)endSearching:(UITapGestureRecognizer *)sender {
    if (![[sender view] isKindOfClass:[UITableView class]]) {
        [self.searchTextField resignFirstResponder];
        [sender.view removeFromSuperview];
        _darkOpaqueView = nil;
    }
}

- (void)settingSearchTextField {
    self.searchTextField = [[MLPAutoCompleteTextField alloc]
                            initWithFrame:[self getRectForSearchTextField]];
    [self.searchTextField setBorderStyle:UITextBorderStyleRoundedRect];
    self.searchTextField.backgroundColor = [UIColor whiteColor];
    self.searchTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.searchTextField.textColor = [UIColor blackColor];
    self.searchTextField.returnKeyType = UIReturnKeySearch;
    self.searchTextField.placeholder = @"Quick searching by name";
    self.searchTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.searchTextField.autoCompleteRowHeight = 70;
    self.searchTextField.reverseAutoCompleteSuggestionsBoldEffect = YES;
    self.searchTextField.shouldResignFirstResponderFromKeyboardAfterSelectionOfAutoCompleteRows = YES;
    
    self.searchTextField.delegate = self;
    self.searchTextField.autoCompleteDelegate = self;
    self.searchTextField.autoCompleteDataSource = self;
}

- (CGRect)getRectForSearchTextField {
    CGRect frame;
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    
    frame.origin.x = screenWidth / 8;
    frame.origin.y = self.navigationController.navigationBar.frame.size.height + 65;
    frame.size.width = (screenWidth / 2 - screenWidth / 8) * 2;
    frame.size.height = 50;
    
    return frame;
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:ExploreShowPersonSegueIdentifier]) {
        PersonViewController *pvc = (PersonViewController *)segue.destinationViewController;
        
        PersonEntity *entity = (PersonEntity *)sender;
        pvc.title = entity.name;
        pvc.person = entity;
    }
}

@end
