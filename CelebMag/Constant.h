//
//  Constant.h
//  GenerateDB
//
//  Created by minhnh on 11/7/14.
//  Copyright (c) 2014 minhnh. All rights reserved.
//

#ifndef GenerateDB_Constant_h
#define GenerateDB_Constant_h

#define BOLD_TITLE_FONT @"DamascusBold"
#define NUM_OF_RND_IMG 20

#define CHECK_UPDATE_LINK @"http://172.245.22.156:81/updateJson"

#define LINK_SERVER_IMAGE_ORIGINAL @"http://172.245.22.156:8080/TestImageApp/ORIGINAL/"
#define LINK_SERVER_IMAGE_THUMB @"http://172.245.22.156:8080/TestImageApp/THUMB/"
#define LINK_SERVER_IMAGE_MEDIUM @"http://172.245.22.156:8080/TestImageApp/MEDIUM/"

#define ABOUT_VC_STORYBOARD_ID @"ABOUT_VIEW_CONTROLLER"
#define ABOUT_US_VIEW @"ABOUT_US_VIEW"
#define LICENSE_VIEW @"LICENSE_VIEW"
#define ABOUT_APP_CELL @"ABOUT_APP_CELL"

#define KEY_FOR_UPDATE_TIME @"UpdateTime"

#endif
