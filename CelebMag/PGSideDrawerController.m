//
//  PGSideDrawerController.m
// SideDrawerExample
//
//  Created by Pulkit Goyal on 18/09/14.
//  Copyright (c) 2014 Pulkit Goyal. All rights reserved.
//

#import "PGSideDrawerController.h"
#import "DrawMenuCell.h"
#import "UIViewController+MMDrawerController.h"

@interface PGSideDrawerController ()

@property (nonatomic) NSInteger currentIndex;
@property (nonatomic, strong) NSArray *menuArray;

@end

@implementation PGSideDrawerController

- (void)viewDidLoad {
	[super viewDidLoad];
	
	self.menuArray = @[@"All", @"New", @"Hot", @"Favorite", @"About Us"];
	self.currentIndex = 0;
	
	self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"default-bg.jpg"]];
}

#pragma mark - Table View datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [self.menuArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	DrawMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MenuCell" forIndexPath:indexPath];
	if (cell) {
		cell.menuLabel.text = self.menuArray[indexPath.row];
		cell.menuImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png", self.menuArray[indexPath.row]]];
	}
	
	return cell;
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (self.currentIndex == indexPath.row) {
		[self.mm_drawerController closeDrawerAnimated:YES completion:nil];
		return;
	}
	
	UIViewController *centerViewController = nil;
	
	switch (indexPath.row) {
		case 0:
			centerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PAGE_VC"];
			break;
		case 1:
			// TODO: Add func later
			centerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NEW_UPDATED_VIEW_CONTROLLER"];
			break;
		case 2:
			// TODO: Add func later
			[[[UIAlertView alloc] initWithTitle:@"This function will coming soon!" message:@"It's under developed and will become available soon to you." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
			break;
		case 3:
			// TODO: Add func later
			[[[UIAlertView alloc] initWithTitle:@"This function will coming soon!" message:@"It's under developed and will become available soon to you." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
			break;
		case 4:
			centerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ABOUT_VIEW_CONTROLLER"];
			break;
		default:
			break;
	}
	
	if (centerViewController) {
		self.currentIndex = indexPath.row;
		[self.mm_drawerController setCenterViewController:centerViewController withCloseAnimation:YES completion:nil];
	} else {
		[self.mm_drawerController closeDrawerAnimated:YES completion:nil];
	}
}

@end
