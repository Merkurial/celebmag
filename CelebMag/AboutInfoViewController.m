//
//  AboutInfoViewController.m
//  CelebMag
//
//  Created by minhnh on 7/18/15.
//  Copyright (c) 2015 minhnh. All rights reserved.
//

#import "AboutInfoViewController.h"

@interface AboutInfoViewController ()

@property (weak, nonatomic) IBOutlet UITextView *aboutUsTextView;

@end

@implementation AboutInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"default-bg.jpg"]];
	[self.aboutUsTextView scrollRangeToVisible:NSMakeRange(0, 1)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)diemDenVietAppTapped:(UIButton *)sender {
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/diem-den-viet/id782438041?mt=8"]];
}

- (IBAction)khachSanVietAppTapped:(UIButton *)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/khach-san-viet/id591416246?mt=8"]];
}

@end
