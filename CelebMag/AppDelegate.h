//
//  AppDelegate.h
//  GenerateDB
//
//  Created by minhnh on 11/1/14.
//  Copyright (c) 2014 minhnh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

