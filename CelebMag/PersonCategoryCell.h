//
//  PersonCategoryCell.h
//  CelebMag
//
//  Created by minhnh on 5/13/16.
//  Copyright © 2016 minhnh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PersonCategoryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *folderImageView;
@property (weak, nonatomic) IBOutlet UILabel *personCategoryNameLabel;

@end
