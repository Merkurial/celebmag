//
//  PersonEntity.m
//  CelebMag
//
//  Created by minhnh on 12/5/14.
//  Copyright (c) 2014 minhnh. All rights reserved.
//

#import "PersonEntity.h"

@interface PersonEntity ()

@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;

@end

@implementation PersonEntity

- (instancetype)initWithPersonId:(NSUInteger)personId name:(NSString *)name biography:(NSString *)biography
{
	self = [super init];
	
	if (self) {
		self.personId = personId;
		self.name = name;
		self.biography = biography;
        NSMutableArray *separatedName = [[self.name componentsSeparatedByString:@" "] mutableCopy];
		if ([separatedName count] == 1) {
			self.firstName = [separatedName lastObject];
			self.lastName = @"";
		} else {
			self.lastName = [separatedName lastObject];
			[separatedName removeLastObject];
			self.firstName = [separatedName componentsJoinedByString:@" "];
		}
	}
	
	return self;
}

@end
