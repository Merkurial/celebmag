//
//  AppDelegate.h
//  CelebMagData
//
//  Created by Dung Nguyen on 1/15/15.
//  Copyright (c) 2015 SIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

