//
//  ViewController.m
//  CelebMagData
//
//  Created by Dung Nguyen on 1/15/15.
//  Copyright (c) 2015 SIG. All rights reserved.
//

#import "PeopleViewController.h"
#import "AddPersonViewController.h"
#import "AddPhotosViewController.h"

@interface PeopleViewController () <AddPersonViewControllerDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation PeopleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"PeoplePresentAddPerson"]) {
        AddPersonViewController *vc = (AddPersonViewController *)[(UINavigationController *)segue.destinationViewController topViewController];
        vc.delegate = self;
    }
}

#pragma mark - UITableViewDataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}

#pragma mark - AddPersonViewControllerDelegate methods

- (void)addPersonViewControllerSelectedCancel:(AddPersonViewController *)vc {
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}

- (void)addPersonViewControllerSelectedDone:(AddPersonViewController *)vc {
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}

@end
