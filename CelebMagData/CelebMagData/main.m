//
//  main.m
//  CelebMagData
//
//  Created by Dung Nguyen on 1/15/15.
//  Copyright (c) 2015 SIG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
