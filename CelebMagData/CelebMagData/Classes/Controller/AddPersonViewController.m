//
//  AddPersonViewController.m
//  CelebMagData
//
//  Created by Dung Nguyen on 1/15/15.
//  Copyright (c) 2015 SIG. All rights reserved.
//

#import "AddPersonViewController.h"

@interface AddPersonViewController ()

@end

@implementation AddPersonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)cancelSelected:(id)sender {
    [self.delegate addPersonViewControllerSelectedCancel:self];
}

- (IBAction)doneSelected:(id)sender {
    [self.delegate addPersonViewControllerSelectedDone:self];
}

@end