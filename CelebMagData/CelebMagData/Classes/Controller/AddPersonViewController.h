//
//  AddPersonViewController.h
//  CelebMagData
//
//  Created by Dung Nguyen on 1/15/15.
//  Copyright (c) 2015 SIG. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddPersonViewControllerDelegate;

@interface AddPersonViewController : UIViewController

@property (weak, nonatomic) id <AddPersonViewControllerDelegate> delegate;

@end


@protocol AddPersonViewControllerDelegate <NSObject>

- (void)addPersonViewControllerSelectedCancel:(AddPersonViewController *)vc;
- (void)addPersonViewControllerSelectedDone:(AddPersonViewController *)vc;

@end